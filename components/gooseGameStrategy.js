export class Board {
    constructor(strategies) {
        this._strategies = strategies;
    }

    getStrategy(position) {
        let strategyFound = this._strategies.find(strategy => strategy._evaluate(position));
        if (strategyFound)
            return strategyFound;
        else
            return new Strategy(position, () => `Stay in space ${position}`);
    }

    getMessagePosition(position) {
        return this.getStrategy(position).doAction();
    }
}

export class Strategy {
    constructor(evaluate, handler) {
        this._evaluate = evaluate;
        this._handler = handler;
    }

    doAction() {
        return this._handler();
    }
}