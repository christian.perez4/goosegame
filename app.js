import {Board, Strategy} from "./components/gooseGameStrategy.js";

const main = async() => {
    const strategies = [
        new Strategy((position) => position === 0, () => 'You are able to Start'),
        new Strategy((position) => position === 6, () => 'The Bridge: Go to space 12'),
        new Strategy((position) => position === 19, () => 'The Hotel: Stay for (miss) one turn'),
        new Strategy((position) => position === 31, () => 'The Well: Wait until someone comes to pull you out - they then take your place'),
        new Strategy((position) => position === 42, () => 'The Maze: Go back to space 39'),
        new Strategy((position) => ((position % 6) === 0 && position !== 6), () => 'Move two spaces forward'),
        new Strategy((position) => position === 58, () => 'Death: Return your piece to the beginning - start the game again'),
        new Strategy((position) => position === 63, () => 'Finish: you ended the game'),
        new Strategy((position) => position > 63, () => 'Move to space 53 and stay in prison for two turns')
    ];
    const board = new Board(strategies);

    console.log('Welcome to Goose Game');
    for(let i = 1; i <= 63; i++) {
        console.log(board.getMessagePosition(i));
    }
}

main();