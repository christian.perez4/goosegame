import {Board, Strategy} from "../components/gooseGameStrategy.js";

describe('testing goose game', () => {

    let board;

    beforeEach(() => {
        const strategies = [
            new Strategy((position) => position === 0, () => 'You are able to Start'),
            new Strategy((position) => position === 6, () => 'The Bridge: Go to space 12'),
            new Strategy((position) => position === 19, () => 'The Hotel: Stay for (miss) one turn'),
            new Strategy((position) => position === 31, () => 'The Well: Wait until someone comes to pull you out - they then take your place'),
            new Strategy((position) => position === 42, () => 'The Maze: Go back to space 39'),
            new Strategy((position) => ((position % 6) === 0 && position !== 6), () => 'Move two spaces forward'),
            new Strategy((position) => position === 58, () => 'Death: Return your piece to the beginning - start the game again'),
            new Strategy((position) => position === 63, () => 'Finish: you ended the game'),
            new Strategy((position) => position > 63, () => 'Move to space 53 and stay in prison for two turns')
        ];
        board = new Board(strategies);
    });

    test('should move 1 position and print Stay in space 1', () => {
        expect(board.getMessagePosition(1)).toBe('Stay in space 1');
    });

    test('should move throw the board with moves 1 and 4 and print the correct messages', () => {
        expect(board.getMessagePosition(1)).toBe('Stay in space 1');

        expect(board.getMessagePosition(5)).toBe('Stay in space 5');
    });

    test('should print The Bridge: Go to space 12 when moving to space 6', () => {
        expect(board.getMessagePosition(6)).toBe(('The Bridge: Go to space 12'));
    });

    test('should print Move two spaces forward for multiples of six (except 6)', () => {
        expect(board.getMessagePosition(12)).toBe('Move two spaces forward');
    });

    test('should print The Hotel message when moving to space 19', () => {
        expect(board.getMessagePosition(19)).toBe('The Hotel: Stay for (miss) one turn');
    });

    test('should print The Well message when moving to space 31', () => {
        expect(board.getMessagePosition(31)).toBe('The Well: Wait until someone comes to pull you out - they then take your place');
    });

    test('should print The Maze message when moving to space 42', () => {
        expect(board.getMessagePosition(42)).toBe('The Maze: Go back to space 39');
    });

    test('should print Death message when moving to space 58', () => {
       expect(board.getMessagePosition(58)).toBe('Death: Return your piece to the beginning - start the game again');
    });

    test('should print Finish message when moving to space 63', () => {
       expect(board.getMessagePosition(63)).toBe('Finish: you ended the game');
    });

    test('should print Prison message when moving to space above to 63', () => {
       expect(board.getMessagePosition(64)).toBe('Move to space 53 and stay in prison for two turns');
    });
});